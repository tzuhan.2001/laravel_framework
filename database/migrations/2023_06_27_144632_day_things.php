<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('day_things', function (Blueprint $table) {
            // control info
            $table->id();
            $table->integer('user_id');
            // basic info
            $table->date('date');
            // additional info
            $table->text('memo')->nullable(); 
            // system
            $table->timestamps();
            $table->unique(['user_id', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('day_things');
    }
};
