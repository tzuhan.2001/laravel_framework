<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {// text -> json 
        Schema::table('day_things', function (Blueprint $table) {
            $table->dropColumn('memo');
        });
        Schema::table('day_things', function (Blueprint $table) {
            $table->json('memo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {// json -> text
        Schema::table('day_things', function (Blueprint $table) {
            $table->text('memo')->nullable()->change();
        });
    }
};
