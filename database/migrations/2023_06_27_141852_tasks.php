<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            // control info
            $table->id();
            $table->string('code')->nullable();
            $table->integer('root');
            $table->integer('parent')->nullable();
            $table->integer('level')->nullable();
            $table->integer('user_id');
            // basic info
            $table->string('name');
            $table->text('description')->nullable();
            $table->date('begin_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('priority')->default(1); // 0: low, 1: medium, 2: high
            // status
            $table->decimal('progress',$precision = 3, $scale = 2)->default(0); // 0% ~ 100%
            $table->integer('status')->default(0); // 0: todo; 1: done; 2:cancel
            $table->timestamp('done_at')->nullable();
            $table->text('comment')->nullable();
            // system
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
