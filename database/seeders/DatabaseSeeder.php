<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);

        \App\Models\Task::factory(20)->create();
        //update all tasks' score
        for ($i=9;$i<20;$i++)
            updateProgress($i);

        \App\Models\Todo::factory(20)->create();
        \App\Models\Todo::where('user_id',"<",0) -> delete();
        \App\Models\DayThing::factory(20)->create();
        \Artisan::call('app:taiwan-calendar');
        
        
    }
}
