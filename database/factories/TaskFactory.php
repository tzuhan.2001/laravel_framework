<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Task;

class TaskFactory extends Factory
{
    static $id = 0;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $begin_date = fake()->dateTimeBetween('-1 week', 'now');
        $end_date = fake()->dateTimeBetween($begin_date, '+6 week');
        if(TaskFactory::$id <3){
            $root = 0;
            $level = 0;
            $parent = 0;
        } else if (TaskFactory::$id < 3*3){
            $root = TaskFactory::$id%3+1;
            $level = 1;
            $parent = $root;
        } else {
            $root = TaskFactory::$id%3+1;
            $level = 2;
            $parent = $root+fake()->numberBetween(1,2)*3;
        }
        TaskFactory::$id++;
        // $root = (TaskFactory::$id++ <3)? 0 : fake()->randomElement([1,2,3]);
        // $level = ($root == 0) ? 0: fake()->numberBetween(1,2);
        // $parent = ($root == 0 || $level == 1) ? $root:fake()->numberBetween(1,10);
        $status = fake()->randomElement([0,1]);
        $done_at = ($status == 0) ? null : fake()->dateTimeBetween($begin_date, 'now');
        
        $comment = array();
        for ($i = 0; $i < fake()->numberBetween(1,3); $i++) {
            $comment[]= fake()->name;
        }
        return [
            'root' => $root,
            'parent' => $parent,
            'level' => $level,
            'user_id' => 1,
            'name' => fake()->name,
            'description' => fake()->text,
            'begin_date' => $begin_date,
            'end_date' => $end_date,
            'progress' => ($status == 1) ? 1 :fake()->randomFloat(2,0,1),
            'priority' => fake()->randomElement([0,1,2]),
            'status' => $status,
            'done_at' => $done_at,
            'comment' => $comment,
        ];
    }
}
