<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DayThingFactory extends Factory
{
    static $factory_dates=[];
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user_id = fake()->numberBetween(1,2);
        $cnt = 0;
        do {
            if ($cnt++ > 10) {
                $date = fake()->format('Y-m-d');
            }
            $date = fake()
                ->dateTimeBetween('-1 week', '+3 week')
                ->format('Y-m-d');
        } while (in_array($date, DayThingFactory::$factory_dates));
        DayThingFactory::$factory_dates[] = $date;

        $memo = array();
        for ($i = 0; $i < fake()->numberBetween(1,3); $i++) {
            $memo[]= fake()->sentence();
        }

        $ret = [
            'user_id' => $user_id,
            'date' => $date,
            // 'memo' => fake()->text(), // for text memo
            'memo' => $memo, // for json memo
        ];
        return $ret;
    }
}
