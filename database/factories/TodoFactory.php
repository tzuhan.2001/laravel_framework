<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Task;
use App\Models\Todo;

class TodoFactory extends Factory
{
    static $fail_id = -1;
    static $factory_dates=[];
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $fail_ret = [
            'user_id' => TodoFactory::$fail_id --,
            'date' => fake()->date(),
        ];
        $user_id = fake()->numberBetween(1,2);
        $tasks = Task::where('user_id', $user_id)
            ->where('level', 2)
            ->inRandomOrder()
            ->limit(3)
            ->get();
        $task1st = $tasks -> first();
        if (empty($task1st))
            return $fail_ret;
        $cnt = 0;
        do {
            if ($cnt++ > 10){
                $shift = $cnt-10;
                $start_date = $task1st -> begin_date." +$shift days";
                $end_date = $task1st -> end_date." +$shift weeks";
                $date = fake()->dateTimeBetween($start_date, $end_date)->format('Y-m-d');
            }
            else
                $date = fake()->dateTimeBetween($task1st -> begin_date, $task1st -> end_date)->format('Y-m-d');
        } while (in_array($date, TodoFactory::$factory_dates));
        TodoFactory::$factory_dates[] = $date;
        $ret = [
            'user_id' => $user_id,
            'tasks' => $tasks->pluck(['id'])->values()->toArray(),
            'date' => $date,
        ];
        $todo = Todo::where('user_id',$user_id)
                    ->where('date', $date)
                    ->first(); // model or null
        // dd([$ret, $todo,empty($todo)]);
        if (!empty($todo)) {
            return $fail_ret;
        }
        return $ret;
    }
}
