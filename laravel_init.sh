#!/bin/sh

composer install
#if APP_KEY in .env is empty, then generate a new key
if awk -F= '$1=="APP_KEY" { if($2=="") print "empty"; else print "exist"}' .env | grep -q 'empty'; then
    php artisan key:generate
fi
#generate jwt secret
if awk -F= '$1=="JWT_SECRET" { if($2=="") print "empty"; else print "exist"}' .env | grep -q 'empty'; then
    php artisan jwt:secret -f
fi
#check if the DB_CONNECTION is empty
if awk -F= '$1=="DB_CONNECTION" { if($2=="") print "empty"; else print "exist"}' .env | grep -q 'exist'; then
    #ping test
    i=0
    #check if the mysql is ready
    #if ready exit the loop (&& false: skip loop; || true: keep loop)
    php artisan db:monitor
    while [ $? -ne 0 ]; do
        if [ $i -eq 10 ]; then
            echo "mysql is not ready"
            exit 1
        fi
        i=$((i+1))
        sleep 3
        php artisan db:monitor
    done
    php artisan migrate
    php artisan app:taiwan-calendar
fi
if php artisan db:monitor; then
    # app_host=`awk -F= '$1=="APP_URL" {print $2}' .env`
    # app_port=`awk -F[=#] '$1=="APP_PORT" {print $2}' .env`
    php artisan serve --host=0.0.0.0 --port=80
fi
