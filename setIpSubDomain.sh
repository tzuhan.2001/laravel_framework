#!/bin/sh
subdomain='xxx.mooo.com'
username="user"
password="password"
hostip=`hostname -I| awk '{print $1;}'`
curl https://${username}:${password}@freedns.afraid.org/nic/update?hostname=${subdomain}\&myip=${hostip}
echo https://${username}:${password}@freedns.afraid.org/nic/update?hostname=${subdomain}\&myip=${hostip}
sed -i "s/APP_URL=.*/APP_URL=http:\/\/$subdomain/" .env
#ignore this file
git update-index --assume-unchanged setIpSubDomain.sh