<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
use App\Http\Controllers\API\JWTAuthController;
use App\Http\Controllers\DayThingController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\DebugController;
use Laravel\Socialite\Facades\Socialite;
 
// define only once
if ( !function_exists("IvyAPIs")) { 
    function IvyAPIs() {
        return [
            Route::get('logout', [JWTAuthController::class, 'logout']),
            //RESTlike
            Route::get('task',[TaskController::class, 'index']),
            Route::get('task/{task}',[TaskController::class, 'show']),
            Route::post('task',[TaskController::class, 'store']),
            
            //Project
            Route::get('project',[ProjectController::class, 'project_index']),
            Route::get('project/{project}/',[ProjectController::class, 'project_show'])
                ->whereNumber(['project']),
            Route::post('project',[ProjectController::class, 'project_store']),
            Route::put('project/{project}',[ProjectController::class, 'project_update']),
            Route::delete('project/{project}',[ProjectController::class, 'project_destroy']),

            //Project Mission
            Route::get('project/mission',[ProjectController::class, 'project_mission_index']),
            Route::get('project/mission/{mission}',[ProjectController::class, 'project_mission_show']),
            Route::post('project/mission',[ProjectController::class, 'project_mission_store']),
            Route::put('project/mission/{mission}',[ProjectController::class, 'project_mission_update']),
            Route::delete('project/mission/{mission}',[ProjectController::class, 'project_mission_destroy']),
            Route::put('project/mission/{mission}/actions',[ProjectController::class, 'project_mission_update_actions']),

            //Project Action
            Route::get('project/action',[ProjectController::class, 'project_action_index']),
            Route::post('project/action',[ProjectController::class, 'project_action_store']),
            Route::get('project/action/{action}', [ProjectController::class, 'project_action_show']),
            Route::put('project/action/{action}',[ProjectController::class, 'project_action_update']),
            Route::delete('project/action/{action}',[ProjectController::class, 'project_action_destroy']),

            //Project Progress
            Route::get('project/progress',[ProjectController::class, 'project_progress_index']),
            Route::get('project/{project}/progress',[ProjectController::class, 'project_progress_show']),

            //Todo
            Route::get('todo/{year?}/{month?}/{day?}', [TodoController::class, 'index'])
                ->whereNumber(['year','month','day']),
            Route::get('todo/shoulddo/{date?}', [TodoController::class, 'shouldDo_index']),
            Route::get('todo/readyto/{date?}', [TodoController::class, 'readyTo_index'])
                ->where('date','[0-9]+\-[01]?[0-9]\-[0-3]?[0-9]')
                ,
            Route::get('todo/readyto/recommend/{date?}', [TodoController::class, 'shouldDo_index_recommend']),
            Route::get('todo/done/{date?}', [TodoController::class, 'done_index']),

            Route::put('todo/{date?}', [TodoController::class, 'update']),
            Route::put('todo/readyto/{date?}', [TodoController::class, 'readyTo_update']),
            Route::put('todo/done/{action}', [TodoController::class, 'done_update']),


            //Calendar
            Route::get('calendar/',[CalendarController::class, 'index']),
            Route::get('calendar/{year}/{month?}/{day?}',[CalendarController::class, 'show'])
                ->whereNumber(['year','month','day'])
                ,

            //Calendar Thing (done, memo)
            Route::get('calendar/thing/{year?}/{month?}/{day?}', [DayThingController::class,'show'])
                ->whereNumber(['year','month','day'])
                ,
            Route::get('calendar/thing/simple/{year?}/{month?}/{day?}', [DayThingController::class,'simple_show'])
                ->whereNumber(['year','month','day'])
                ,
            Route::put('calendar/thing/{date?}', [DayThingController::class, 'update']),
        ];}
}

// Google OAuth2.0 process
// 1. Get token from Google OAuth2.0
// 2. auth.google will check the token with google API
// 3. able to use Ivy method APIs

Route::group(['middleware' => ['auth.google'], 'prefix' => 'gauth/'], function () {[
    Route::group(['prefix' => 'test/'], function (){
        Route::get('pass',function(){return "api test pass";});
    }),
    Route::group([], function (){
        Route::get('user',function(Request $request){
            $token = $request->bearerToken();
            $guser = Socialite::driver('google')->userFromToken($token);
            return response()->json($guser->attributes);
            return auth()->user();
        });
    }),
    ...IvyAPIs()
    ];
});

// JWT process
// 1. get token by login actions
// 2. auth.jwt will check the token
// 3. able to use Ivy method APIs

Route::post('register', [JWTAuthController::class, 'register']);
Route::post('login', [JWTAuthController::class, 'login']);
Route::group(['middleware' => ['token.refresh',]], function() {[
        Route::get('refreshToken', [JWTAuthController::class, 'refreshToken']),
        Route::get('refreshToken_test', [JWTAuthController::class, 'refreshToken_test']),
    ];
});

Route::group(['middleware' => 'auth.jwt'], function () {[
 
        Route::group(['prefix' => 'test/'], function (){
            Route::get('pass',function(){return "api test pass";});
            Route::put('check_input', [DebugController::class, 'check_input']);
            Route::get('wrong_SQL_test', [DebugController::class, 'wrong_SQL_test']);
        }),
        ...IvyAPIs()
    ];
});
