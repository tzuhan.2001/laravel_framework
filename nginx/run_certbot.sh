#!/bin/bash
#should be run as root
DOMAIN_NAME=$1
EMAIL=$2
HOMEUSER=$3
PROJECT=$4


if [ -z "$DOMAIN_NAME" ]; then
    echo "DOMAIN_NAME is required"
    exit 1
fi
if [ -z "$EMAIL" ]; then
    echo "EMAIL is required"
    exit 1
fi
#check if certbot is installed
if ! command -v certbot &> /dev/null
then
    echo "certbot could not be found"
    exit 1
fi
#check if 80 port is free
if [ "$(lsof -i :80)" ]
then
    echo "Port 80 is in use"
    #check if cert is already installed
    if [ -d "/etc/letsencrypt/live/${DOMAIN_NAME}" ]
    then
        echo "Cert is already installed"
        certbot renew
    else
        echo "Cert is not installed"
        echo certbot certonly --webroot -w /var/www/html/ --email ${EMAIL} --agree-tos --preferred-challenges http -d ${DOMAIN_NAME}
        certbot certonly --webroot -w /var/www/html/ --email ${EMAIL} --agree-tos --preferred-challenges http -d ${DOMAIN_NAME}
    fi
else
    echo "Port 80 is free"
    #check if cert is already installed
    if [ -d "/etc/letsencrypt/live/${DOMAIN_NAME}" ]
    then
        echo "Cert is already installed"
        certbot renew --standalone
    else
        echo "Cert is not installed"
        echo certbot certonly --standalone --email ${EMAIL} --agree-tos --preferred-challenges http -d ${DOMAIN_NAME}
        certbot certonly --standalone --email ${EMAIL} --agree-tos --preferred-challenges http -d ${DOMAIN_NAME}
    fi
fi
echo "Copying cert files to project: /home/${HOMEUSER}/${PROJECT}/nginx/letsencrypt/ivy"
cp /etc/letsencrypt/live/${DOMAIN_NAME}/fullchain.pem /home/${HOMEUSER}/${PROJECT}/nginx/letsencrypt/ivy
cp /etc/letsencrypt/live/${DOMAIN_NAME}/privkey.pem /home/${HOMEUSER}/${PROJECT}/nginx/letsencrypt/ivy
exit 1
