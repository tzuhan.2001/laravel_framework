<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class Todo extends Model
{
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
    use HasFactory;
    protected $guarded = [
        'id'
    ];
    // protected $table = "day_things";


    // protected $columns = [
    //     'todos',
    //     'date',
    //     'isHoliday',
    //     'reason',
    //     'memo',
    // ];
    protected $casts = [
        'tasks' => 'json',
    ];
    public function task_info()
    {
        return $this->belongsToJson(Task::class, 'tasks');
    }
    public function tasks()
    {
        return $this->belongsToJson(Task::class, 'tasks');
    }
    public function scopeUser(Builder $query):void
    {
        $query->where('user_id', auth()->user()->id);
    }
    static function getShouldDo($date): collection //collection of Action
    {
        try {
            # dont need to show the tasks(actions) that have been assigned
            $assigned = Todo::user()
                -> whereDate('date', $date)
                -> pluck('tasks')
                -> flatten()
                ;
            $shouldDo = Action::user()
                -> where('status', 0)
                -> whereNotIn('id', $assigned)
                -> with(['root' => function($query) {
                        $query->select(['id','name'])->get();
                    }])
                -> get()
                ;
            return $shouldDo;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    static function getReadyTo($date): collection //collection of Todo
    {
        try {
            $readyTo = Todo::user()
                -> whereDate('date', $date)
                -> with(['task_info' => function($query) {
                    $query
                        ->where('status',0)
                        ->with(['root' => function($query) {
                            $query->select(['id','name'])->get();}
                        ]);
                }])
                -> get()
                ;
            return $readyTo;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    static function getDone($date) //collection of Todo
    {
        try {
            $done = Todo::user()
                -> whereDate('date', $date)
                -> with(['task_info' => function($query) {
                    $query
                        ->where('status',1)
                        ->with(['root' => function($query) {
                            $query->select(['id','name'])->get();}
                        ]);;
                }])
                -> get()
                -> pluck('task_info') -> flatten() //only show task_info no todo_id
                ;
            return $done;
        } catch (\Exception $e) {
            throw $e;
        }
    }
    static function getDoneYMD($year, $month = null, $day=null): collection //collection of Todo
    {
        try {
            $done = Todo::user()
                // if year is null, then ignore year
                -> when($year, function ($query, $year) {
                    return $query->whereYear('date', $year);
                })
                // if month is null, then ignore month
                -> when($month, function ($query, $month) {
                    return $query->whereMonth('date', $month);
                })
                // if day is null, then ignore day
                -> when($day, function ($query, $day) {
                    return $query->whereDay('date', $day);
                })
                -> with(['task_info' => function($query) {
                    $query->where('status',1);
                }])
                -> get()
                ;
            return $done;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
