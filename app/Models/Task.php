<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Validator;

class Task extends Model
{
    use HasFactory;
    
    protected $guarded = [
        'id',
        // 'root',
        // 'parent',
        // 'done_at',
    ];
    protected $dates = ['begin_date', 'end_date', 'done_at'];
    protected $casts = [
        'comment' => 'array',
    ];
    // protected $fillable=[
    //     "code",
    //     "name",
    //     "description",
    //     "begin_date",
    //     "end_date",
    //     "priority",
    //     "status",
    //     "comment",
    // ];
    public function root()
    {
       return $this->belongsTo(Task::class, 'root');
    }
    public function parent()
    {
       return $this->belongsTo(Task::class, 'parent');
    }
    public function children():HasMany
    {
       return $this->HasMany(Task::class, 'parent', 'id');
    }
    public function scopeUser(Builder $query):void
    {
        $query->where('user_id', auth()->user()->id);
    }

    public function scopeTree(Builder $query, int $root):void
    {
        $query->where('root',$root);
    }

    public function scopeActive(Builder $query):void
    {
        $query->where('status',0);
    }

    public function updateProgress(Builder $query):void
    {
        $query->where('status',0);
    }

}