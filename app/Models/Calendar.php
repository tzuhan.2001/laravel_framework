<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    use HasFactory;
    protected $hidden =[
        "created_at",
        "updated_at",
    ];
    static function getHolidaysYMD($year, $month=NULL, $day=NULL)
    {
        try {
            $holiday = Calendar::
                // if year is null, then ignore year
                when($year, function ($query, $year) {
                    return $query->whereYear('date', $year);
                })
                // if month is null, then ignore month
                -> when($month, function ($query, $month) {
                    return $query->whereMonth('date', $month);
                })
                // if day is null, then ignore day
                -> when($day, function ($query, $day) {
                    return $query->whereDay('date', $day);
                })
                -> get()
                ;
            return $holiday;
        } catch (\Exception $e) {
            return null;
        }
    }
}
