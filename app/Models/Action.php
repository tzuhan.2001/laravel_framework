<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Action extends Task
{
    use HasFactory;
    //table name
    protected $table = 'tasks';
    protected $guarded = [
        'id',
        'level',
    ];
    // the level of the action should be 1
    // the root of the action should be the id of the project
    // the parent of the action should be the id of the mission
    // and the root of action should be the same as the parent of the parent
    protected $attributes=[
        'level'=>2,
    ];
    function project():BelongsTo
    {
        return $this->belongsTo(Project::class, 'root');
    }
    function mission():BelongsTo
    {
        return $this->belongsTo(Mission::class, 'parent');
    }
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('level', function (Builder $builder) {
            $builder->where('level', 2);
        });

        static::creating(function ($action) {
            // Get project and mission that this action belongs to
            $project = $action->project;
            $mission = $action->mission;
            $action->user_id = auth()->user()->id;

            // If the action has a parent and the parent's project is not the same as this action's project
            if (empty($mission) || empty($project)) {
                throw new \InvalidArgumentException('The root/parent of the action must be a project/mission.');
            } else if ($mission->parent !== $project->id) {
                throw new \InvalidArgumentException('The root of action and root of mission should be the same.');
            }
            $action->begin_date ??= $mission->begin_date;
            $action->end_date ??= $mission->end_date;
            $validated = validator($action->toArray(), [
                'root'          =>'required|integer',
                'parent'        =>'required|integer',
                'level'         =>'required|integer', // 0: project; 1: mission; 2: action
                'user_id'       =>'required|integer',
                'name'          =>'required|string',
                'description'   =>'nullable|string',
                'begin_date'    =>'required|date',
                'end_date'      =>'required|date',
            ]);
            
            if ($validated->fails()) {
                throw new \InvalidArgumentException( $validated->errors()->first());
            } else if ( $action->begin_date > $action->end_date) {
                throw new \InvalidArgumentException('The begin_date of the action must be earlier than the end_date.');
            } else if ( $action->end_date < date('Y-m-d')
                        && getenv('LOOSE_ACTION_DATE')=='0'
                        ){
                throw new \InvalidArgumentException('The end_date of the project must be later than today.'.date('Y-m-d'));
            } else if ( $action->begin_date < $mission->begin_date
                        && getenv('LOOSE_ACTION_DATE')=='0'
                        ){
                throw new \InvalidArgumentException('The begin_date of the action must be later than the begin_date of the mission.');
            } else if ( $action->end_date > $mission->end_date
                        && getenv('LOOSE_ACTION_DATE')=='0'
                        ){
                throw new \InvalidArgumentException('The end_date of the action must be earlier than the end_date of the mission.');
            }
        });

        static::updating(function ($action) {
            if ( $action->user_id !== auth()->user()->id) {
                throw new \InvalidArgumentException('The user_id of the action must be the same as the auth user.');
            } else if ( $action->begin_date > $action->end_date) {
                throw new \InvalidArgumentException('The begin_date of the action must be earlier than the end_date.');
            } else if ( $action->isDirty('end_date') && $action->end_date < date('Y-m-d')
                        && getenv('LOOSE_ACTION_DATE')=='0'
                        ){
                throw new \InvalidArgumentException('The end_date of the action must be later than today.'.date('Y-m-d'));
            } else if ( $action->begin_date < $action->mission->begin_date
                        && getenv('LOOSE_ACTION_DATE')=='0'
                        ){
                throw new \InvalidArgumentException('The begin_date of the action must be later than the begin_date of the mission.');
            } else if ( $action->end_date > $action->mission->end_date
                        && getenv('LOOSE_ACTION_DATE')=='0'
                        ){
                throw new \InvalidArgumentException('The end_date of the action must be earlier than the end_date of the mission.');
            }
        });
    }

}