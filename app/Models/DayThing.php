<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DayThing extends Model
{
    use HasFactory;
    protected $guarded = [
        'id'
    ];
    // protected $table = "day_things";


    // protected $columns = [
    // ];
    protected $casts = [
        'memo' => 'array',
    ];

    public function scopeUser(Builder $query):void
    {
        $query->where('user_id', auth()->user()->id);
    }
    static function getMemoYMD($year, $month, $day)
    {
        try {
            $memo = DayThing::user()
                // if year is null, then ignore year
                -> when($year, function ($query, $year) {
                    return $query->whereYear('date', $year);
                })
                // if month is null, then ignore month
                -> when($month, function ($query, $month) {
                    return $query->whereMonth('date', $month);
                })
                // if day is null, then ignore day
                -> when($day, function ($query, $day) {
                    return $query->whereDay('date', $day);
                })
                -> get()
                ;
            return $memo;
        } catch (\Exception $e) {
            return null;
        }
    }
}
