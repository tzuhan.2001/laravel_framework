<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Mission extends Task
{
    use HasFactory;
    //table name
    protected $table = 'tasks';
    protected $guarded = [
        'id',
        'level',
    ];
    // the level of the mission should be 1
    // the root of the mission should be the id of the project
    // the parent of the mission should be the id of the project
    // and the root of mission should be the same as the parent of the mission
    protected $attributes=[
        'level'=>1,
    ];
    function project():BelongsTo
    {
        return $this->belongsTo(Project::class, 'root');
    }
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('level', function (Builder $builder) {
            $builder->where('level', 1);
        });

        static::creating(function ($mission) {
            // Get the project that this mission belongs to
            $project = $mission->project;
            // Add user_id
            $mission->user_id = auth()->user()->id;

            if (empty($project)) {
                throw new \InvalidArgumentException('The root is not a project.');
            } else if ($mission->parent !== $mission->root) {
                throw new \InvalidArgumentException('The root/parent of the mission must be a project and should be the same value.');
            }

            $mission->begin_date ??= $project->begin_date;
            $mission->end_date ??= $project->end_date;
            $validated = validator($project->toArray(), [
                'root'          =>'required|integer',
                'parent'        =>'required|integer',
                'level'         =>'required|integer', // 0: project; 1: mission; 2: action
                'user_id'       =>'required|integer',
                'name'          =>'required|string',
                'description'   =>'nullable|string',
                'begin_date'    =>'required|date',
                'end_date'      =>'required|date',
            ]);
            
            if ($validated->fails()) {
                throw new \InvalidArgumentException( $validated->errors()->first());
            } else if ( $mission->begin_date > $mission->end_date) {
                throw new \InvalidArgumentException('The begin_date of the mission must be earlier than the end_date.');
            } else if ( $mission->end_date < date('Y-m-d')) {
                throw new \InvalidArgumentException('The end_date of the project must be later than today.'.date('Y-m-d'));
            } else if ( $mission->begin_date < $project->begin_date) {
                throw new \InvalidArgumentException('The begin_date of the mission must be later than the begin_date of the project.');
            } else if ( $mission->end_date > $project->end_date) {
                throw new \InvalidArgumentException('The end_date of the mission must be earlier than the end_date of the project.');
            }
        });

        static::updating(function ($mission) {
            if ( $mission->user_id !== auth()->user()->id) {
                throw new \InvalidArgumentException('The user_id of the mission must be the same as the auth user.');
            } else if ( $mission->parent !== $mission->root) {
                throw new \InvalidArgumentException('The root/parent of the mission must be a project and should be the same value.');
            } else if ( $mission->begin_date > $mission->end_date) {
                throw new \InvalidArgumentException('The begin_date of the mission must be earlier than the end_date.');
            } else if ( $mission->isDirty('end_date') && $mission->end_date < date('Y-m-d')) {
                throw new \InvalidArgumentException('The end_date of the mission must be later than today.'.date('Y-m-d'));
            } else if ( $mission->begin_date < $mission->project->begin_date) {
                throw new \InvalidArgumentException('The begin_date of the mission must be later than the begin_date of the project.');
            } else if ( $mission->end_date > $mission->project->end_date) {
                throw new \InvalidArgumentException('The end_date of the mission must be earlier than the end_date of the project.');
            }
            // #special case: action date is updated as mission date
            // $actions = Action::where('parent', $mission->id)->get();
            // foreach ($actions as $action){
            //     $action->update([
            //         'begin_date' => max($mission->begin_date , $action->begin_date),
            //         'end_date' => min($mission->end_date , $action->end_date),
            //     ]); 
            // }
        });
    }

}