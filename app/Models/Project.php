<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
class Project extends Task
{
    use HasFactory;
    //table name
    protected $table = 'tasks';
    
    protected $guarded = [
        'id',
        'level',
        'root',
        'parent',
    ];
    //the level of the project should be 0
    protected $attributes=[
        'level'=>0,
        'root'=>0,
        'parent'=>0,
    ];
    protected static function boot()
    {
        
        parent::boot();

        static::addGlobalScope('level', function (Builder $builder) {
            $builder->where('level', 0);
        });
    
        static::creating(function ($project) {
            
            // add user_id
            $project->user_id = auth()->user()->id;
            // the root/parent value of the project should be 0
            if ( $project->parent !== 0 || $project->root !== 0) {
                throw new \InvalidArgumentException('The root/parent of the project must be 0.');
            }
            $validated = validator($project->toArray(), [
                'root'          =>'required|integer',
                'parent'        =>'required|integer',
                'level'         =>'required|integer', // 0: project; 1: mission; 2: action
                'user_id'       =>'required|integer',
                'name'          =>'required|string',
                'description'   =>'nullable|string',
                'begin_date'    =>'required|date',
                'end_date'      =>'required|date',
            ]);

            if ($validated->fails()) {
                throw new \InvalidArgumentException( $validated->errors()->first(),);
            } else if ( $project->begin_date > $project->end_date) {
                throw new \InvalidArgumentException('The begin_date of the project must be earlier than the end_date.');
            } else if ( $project->end_date < date('Y-m-d')) {
                throw new \InvalidArgumentException('The end_date of the project must be later than today.'.date('Y-m-d'));
            }
        });

        static::updating(function ($project) {
            if ( $project->user_id !== auth()->user()->id) {
                throw new \InvalidArgumentException('The user_id of the project must be the same as the auth user.');
            } else if ( $project->parent !== 0 || $project->root !== 0) {
                throw new \InvalidArgumentException('The root/parent of the project must be 0.');
            } else if ( $project->begin_date > $project->end_date) {
                throw new \InvalidArgumentException('The begin_date of the project must be earlier than the end_date.');
            } else if ( $project->isDirty('end_date') && $project->end_date < date('Y-m-d')) {
                throw new \InvalidArgumentException('The end_date of the project must be later than today.'.date('Y-m-d'));
            }
            // #special case: mission date is updated as project date
            // $missions = Mission::where('parent', $project->id)->get();
            // foreach ($missions as $mission) {
            //     $mission->begin_date = $project->begin_date;
            //     $mission->end_date = $project->end_date;
            //     $mission->save();
            // }
        });
    }
    
    function mission(): HasMany
    {
        return $this->hasMany(Mission::class, 'root');
    }
    function action(): HasMany
    {
        return $this->hasMany(Action::class, 'root');
    }
}