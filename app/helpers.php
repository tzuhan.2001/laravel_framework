<?php
use App\Models\Task;
/**
 * check score of a Task (the higher the Important/Urgent)
 * Should has following attributes:
 * positive with $priority, $runDay, $setupDay
 * negative with $progress, $restDay,
 * $runDay = now() - begin_date
 * $restDay = end_date - now()
 * $setupDay = end_date - begin_date
 */
// function scoreOfTask($begin_date, $end_date, $priority, $progress, $method = "debt"){
function scoreOfTask($task, $method = "debt"){
    
    $begin_date = $task->begin_date ?? $task->parent->$begin_date;
    $end_date = $task->end_date ?? $task->parent->$end_date;
    $priority = $task->priority;
    $progress = $task->progress;
    if ($progress == 1)
        return -INF;
    $now = new \DateTime();
    $begin = new \DateTime($begin_date);
    $end = new \DateTime($end_date);
    $runDay = date_diff($begin, $now)->format('%r%a'); // get "(-)${days}"
    $restDay = date_diff($now, $end)->format('%r%a'); // get "(-)${days}"
    $setupDay = date_diff($begin, $end)->format('%r%a'); // get "(-)${days}"
    $setupDay = max($setupDay, 1);
    /**
     * deadline method
     */
        $deadlineDistance = $setupDay - $restDay;
        $deadlineDistance += $deadlineDistance*$priority;
    /**
     * debt method
     * $priority: 0=> baseIncome; 1:2*baseIncome; 2:4*baseIncome
     * 
     */
        $baseIncome = 3000;
        // $weightedIncome = $baseIncome;
        $weightedIncome = (2**$priority) * $baseIncome;
        // 日收入 = 總金額 / 安排時程
        $dailyIncome = $weightedIncome / $setupDay;
        // 實際收入 = 完成進度 * 總金額
        $getIncome = $progress * $weightedIncome;
        // debt = 應收 - 實收
        $expectIncome = min( $runDay * $dailyIncome, $weightedIncome );
        $debt = $expectIncome - $getIncome;
        // 罰款
        if($restDay < 0 && $debt > 0){
            $debt += abs($restDay) * $dailyIncome;
        }
        // $debt = $runDay;
    switch ($method) {
        case 'deadline':
            $score = $deadlineDistance;
            break;
        case 'debt':
            $score = $debt;
            break;   
        default:
            $score = $debt;
            break;
    }
    return $score;
}

function progressByChildren($task_id){
    $task = Task::with(['children'])->where('id',$task_id)->first();
    $children_progress = 0;
    $children_cnt = 0;
    foreach ($task->children as $key => $child) {
        if ($child->status != 2) {
            $children_progress+=$child->progress;
            $children_cnt++;
        }
    }
    $progress = ($children_cnt == 0) ? 1*($task->status == 1) : $children_progress/$children_cnt;
    return $progress;
}

function updateProgress($task_id){
    $task = Task::where('id',$task_id)->first();
    // Recursive stop criterion
    if (empty($task))
        return true;
    $progress = progressByChildren($task_id);
    $task->progress = $progress;
    if ($progress >= 1){
        $task->status = 1;
        $task->done_at = now();
    } else {
        $task->status = 0;
    }
    try{
        $task->save();
    } catch (\Exception $e){
        //exception doing outside
        return false;
    }
    // Recursive stop criterion
    if ($task->parent == 0)
        return true;
    // Recursive call parent Task
    return updateProgress($task-> parent);
}
?>