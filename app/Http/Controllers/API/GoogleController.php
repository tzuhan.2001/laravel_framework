<?php
 
namespace App\Http\Controllers\API;
//ref https://www.positronx.io/laravel-9-socialite-login-with-google-example-tutorial/

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;

class GoogleController extends Controller
{
    public function signInwithGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
    public function callbackToGoogle(Request $request)
    {
        try {
            $guser = Socialite::driver('google')->stateless()->user();

            $user = User::where('gauth_id', $guser->id)->first();
            if(!$user){
                $user = User::create([
                    'name' => $guser->name,
                    'email' => $guser->email,
                    'gauth_id'=> $guser->id,
                    'gauth_type'=> 'google',
                    'avatar' => $guser->avatar,
                    'password' => encrypt('BackDoorPasswordShouldNotBeUsed!')
                ]);
            }
            Auth::login($user);
            // return response()->json($user);
            return redirect('/');
     
        } catch (Exception $e) {
            // dd([$e->getMessage());
            dd([$e->getMessage(),$request,$e]);
        }
    }
}