<?php
 
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use JWTAuth;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
 
class JwtAuthController extends Controller
{
    public $token = true;
  
    public function register(Request $request)
    {
 
         $validator = Validator::make($request->all(), 
                      [ 
                      'name' => 'required',
                      'email' => 'required|email',
                      'password' => 'required',  
                      'c_password' => 'required|same:password', 
                     ]);  
 
         if ($validator->fails()) {  
 
               return response()->json(['error'=>$validator->errors()], 401); 
 
            }   
 
 
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
  
        if ($this->token) {
            return $this->login($request);
        }
  
        return response()->json([
            'status' => true,
            'data' => $user
        ], Response::HTTP_OK);
    }
  
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;
        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'status' => false,
                'message' => 'Invalid Email or Password',
            ], Response::HTTP_UNAUTHORIZED);
        }
        JWTAuth::setToken($jwt_token);
        $payload = JWTAuth::getPayload();
        $expirationTime = $payload['exp'];
        return response()->json([
            'status' => true,
            'token' => $jwt_token,
            'exp' => $expirationTime,
            'expSec' => $expirationTime-time(),
        ]);
    }
    public function refreshToken(Request $request)
    {
        $newToken = JWTAuth::refresh(JWTAuth::getToken(), [], config('ttl'));
        JWTAuth::setToken($newToken);
        $payload = JWTAuth::getPayload();
        $expirationTime = $payload['exp'];
        return response()->json([
                'status' => true,
                'token' => $newToken,
                'exp' => $expirationTime,
                'expSec' => $expirationTime-time(),
            ])
            // ->header('Authorization', $newToken);
            // ->headers->set('Authorization', 'QQBearer '.$newToken);
        ;
    }
    public function refreshToken_test(Request $request)
    {
        return response()->json([
            'status' => true,
            'message' => "new JWT is set on header",
        ]);
    }
  
    public function logout(Request $request)
    {   
        try {
            JWTAuth::invalidate($request->token);
  
            return response()->json([
                'status' => true,
                'message' => 'User logged out statusfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
  
    public function getUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
  
        $user = JWTAuth::authenticate($request->token);
  
        return response()->json(['user' => $user]);
    }
}
