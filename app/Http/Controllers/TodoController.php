<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Project;
use App\Models\Mission;
use App\Models\Action;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    protected $validateRulesCreate = [
        'readyTo.*' =>'nullable|exists:App\Models\Action,id',
        'date'  =>'required|date',
    ];
    protected $validateRulesUpdate = [
        'readyTo.*' =>'nullable|exists:App\Models\Action,id',
        'date'  =>'nullable|date',
    ];
    protected $validateMessages = [
        'readyTo.*' =>':attribute should be exists:App\Models\Action,id',
        'date.*'    =>':attribute should be date',
    ];
    public function todo_validate($inputs, $validateType){
        switch ($validateType) {
            case 'create':
                $validateRules = collect($this->validateRulesCreate);
                break;
            case 'update':
                $validateRules = collect($this->validateRulesUpdate);
                break;
            default:
                $validateRules = collect($this->validateRulesCreate);
                break;
        }
        $validator = Validator::make(
            $inputs,
            $validateRules->toArray(),
            $this->validateMessages
        );
        if ($validator->fails()) {
            throw new \InvalidArgumentException($validator->errors()->first());
            $error = $validator->errors()->first();
            return ['status' => false, 'ret' => $error];
        }
        return ['status' => true];
    }
    /**
     * Display a listing of the resource.
     */
    
    public function index(Request $request, $year=NULL, $month=NULL, $day=NULL)
     {
         $request->merge([
             'year' => $year,
             'month' => $month,
             'day' => $day,
         ]);
         $request->validate([
             'year' => 'nullable|date_format:Y',
             'month' => 'nullable|date_format:m',
             'day' => 'nullable|date_format:d',
         ]); 
         $todo_builder = Todo::user()
            -> with(['task_info' => function($query) {
                // $query->where('status',1);
            }]);
         if ($day){
             $todo_builder =  $todo_builder -> whereDate('date',"{$year}-{$month}-{$day}");
         } else if($month){
             $todo_builder =  $todo_builder -> whereYear('date',$year)->whereMonth('date',$month);
         } else if($year){
             $todo_builder =  $todo_builder -> whereYear('date',$year);
         }
        $todo = $todo_builder
            -> get()
            -> map(function($x){ 
                $x->task_done = $x->task_info-> where('status',1)->pluck('id')->flatten();
                $x->task_readyto = $x->task_info-> where('status',0)->pluck('id')->flatten();
                $x->task_cancel = $x->task_info-> where('status',2)->pluck('id')->flatten();
                $x->task_deleted = array_values(array_diff($x->tasks,
                                                $x->task_done->toArray(),
                                                $x->task_readyto->toArray(),
                                                $x->task_cancel->toArray()));
                unset($x->task_info);
                return $x;
            })            
            ;
        return [
            'status' => true,
            'ret' => $todo,
        ];
    }

    public function shouldDo_index($date = 'now')
    {
        $n_day = 5;
        try{
            $date = new \DateTimeImmutable ($date);
            $after_n = $date->add(new \DateInterval("P{$n_day}D"));
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "date format error",
                "message" => $e->getMessage(),
            ];
        }
        try {
            $shouldDo = ToDo::getShouldDo($date)
                -> sortBy('end_date')
                // -> take(6)
                -> values()
                ;
            return [
                'status' => true,
                'ret' => $shouldDo,
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "assigned/shouldDo error",
                "message" => $e->getMessage(),
            ];
        }
    }
    public function shouldDo_recommend($date = 'now')
    {
        $n_day = 5;
        try{
            $date = new \DateTimeImmutable ($date);
            $after_n = $date->add(new \DateInterval("P{$n_day}D"));
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "date format error",
                "message" => $e->getMessage(),
            ];
        }
        try {
            $shouldDo = Todo::getShouldDo($date) // return Action list
                -> transform(function($task){
                    $task['score'] = scoreOfTask($task);
                    return $task;
                    })
                -> sortByDesc('score')
                -> take(6)
                -> transform(function($task){
                    // INF, -INF, NaN are not allowed
                    if ($task['score'] == INF) $task['score'] = 999999;
                    if ($task['score'] == -INF) $task['score'] = -999999;
                    if (is_nan($task['score'])) $task['score'] = -999999;
                    return $task;
                    })
                -> values()
                ;
            return [
                'status' => true,
                'ret' => $shouldDo,
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "recommend error",
                "message" => $e->getMessage(),
            ];
        }
    }
    public function shouldDo_index_recommend($date = 'now')
    {
        try{
            $date = new \DateTimeImmutable ($date);
            $date = $date -> format("Y-m-d");
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "date format error",
                "message" => $e->getMessage(),
            ];
        }
        try {
            //get recommend list
            $shouldDo_ret = $this->shouldDo_recommend($date);
            $shouldDo = $shouldDo_ret['ret'];
            //add recommend list to todo
            $todo = Todo::firstOrNew(['user_id' => auth()->user()->id,'date'=>$date]);
            $tasks = $todo->tasks ?? [];
            $req = [ 'tasks' => array_unique([...$shouldDo->pluck('id')->flatten(), ...$tasks])];
            $todo->fill($req);
            $todo->save();
            return [
                'status' => true,
                'ret' => $shouldDo,
                'debug' => [
                    'shouldDo' => $shouldDo,
                    'tasks' => $tasks,
                ],
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "assigned recommend error",
                "message" => $e->getMessage(),
                "debug" => $shouldDo_ret,
            ];
        }
    }
    public function readyTo_index($date = 'now')
    {
        try{
            $date = new \DateTimeImmutable ($date);
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "date format error",
                "message" => $e->getMessage(),
            ];
        }
        try {
            $readyTo = Todo::getReadyTo($date)
                -> pluck('task_info') -> flatten() //only show task_info no todo_id
                ;
            return [
                'status' => true,
                'ret' => $readyTo,
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "readyTo error",
                "message" => $e->getMessage(),
            ];
        }
    }
    public function done_index($date = 'now')
    {
        try{
            $date = new \DateTimeImmutable ($date);
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "date format error",
                "message" => $e->getMessage(),
            ];
        }
        try {
            $done = Todo::getDone($date);
            return [
                'status' => true,
                'ret' => $done,
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "done error",
                "message" => $e->getMessage(),
            ];
        }
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $date='now')
    {
        try{
            $date = new \DateTimeImmutable ($date);
            $date = $date -> format("Y-m-d");
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "date format error",
                "message" => $e->getMessage(),
            ];
        }
        try {
            //validate
            $validated = $this->todo_validate($request->all(), "update");
            $readyTo = collect($request->get('readyTo'));
            $readyTo = $readyTo -> filter(function($child){
                //only allow user's level 2 of task
                return (Action::user()->where(['id' => $child, 'status' => 0])->exists());
            }) -> values();
            // Update tasks of todo table
            // tasks = readyTo(from request) + done (UI could not send done list)
            $todo = Todo::firstOrNew(['user_id' => auth()->user()->id,'date'=>$date]);
            $done = Todo::getDone($date)->pluck('id')->flatten();
            $req = [ 'tasks' => array_unique([...$readyTo, ...$done])];
            
            $todo->fill($req);
            $todo->save();
            return [
                'status' => true,
                'ret' => $todo,
                'debug' => [
                    'readyTo' => $readyTo,
                    'done' => $done,
                ],
            ];
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'ret' => $e->getMessage(),
                "message" => [
                    'request' => $request->all()
                ],
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "update error",
                "message" => $e->getMessage(),
            ];
        }
    }
    public function readyTo_update(Request $request, $date='now')
    {
        try {
            $date = new \DateTimeImmutable ($date);
            $date = $date -> format("Y-m-d");
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "date format error",
                "message" => $e->getMessage(),
            ];
        }
        try {
            $readyTo = collect($request->all());
            $readyTo = $readyTo -> filter(function($child){
                //only allow user's level 2 of task
                return (Action::user()->where(['id' => $child, 'status' => 0])->exists());
            }) -> values();
            // Update tasks of todo table
            // tasks = readyTo(from request) + done (UI could not send done list)
            $todo = Todo::firstOrNew(['user_id' => auth()->user()->id,'date'=>$date]);
            $done = Todo::getDone($date)->pluck('id')->flatten();
            
            $req = [ 'tasks' => array_unique([...$readyTo, ...$done])];
            
            $todo->fill($req);
            $todo->save();
            return [
                'status' => true,
                'ret' => $todo,
                'debug' => [
                    'readyTo' => $readyTo,
                    'done' => $done,
                ],
            ];
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'ret' => $e->getMessage(),
                "message" => [
                    'request' => $request->all()
                ],
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "update error",
                "message" => $e->getMessage(),
            ];
        }
    }

    public function done_Update(Request $request, Action $action)
    {
        try {
            //only allow "status"
            $others = array_keys($request->except(['status']));
            foreach ($others as $unacceptable) {
                unset($request[$unacceptable]);
            }
            $status = $request['status'];
            switch (\Str::lower($status)) {
                case 'done':
                    $request['status'] = 1;
                    $request['progress'] = 1;
                    break;
                default:
                    $request['status'] = 0;
                    $request['progress'] = 0;
                    break;
            }
            return \App::make('App\Http\Controllers\ProjectController')->project_action_update($request, $action);
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "done_Update error (action, status): ({$action->id}, {$status})",
                "message" => $e->getMessage(),
            ];
        }
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DayThing $dayThing)
    {
        //
    }
}
