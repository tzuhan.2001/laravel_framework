<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DebugController extends Controller
{
    protected $resp = [
        "status" => false,
        "ret" => null
    ];
    protected $validateRulesCreate = [
        'readyTo.*' =>'nullable|exists:App\Models\Task,id',
        'date'  =>'required|date',
    ];
    protected $validateRulesUpdate = [
        'readyTo.*' =>'nullable|exists:App\Models\Task,id',
        'date'  =>'nullable|date',
    ];
    protected $validateMessages = [
        'readyTo.*' =>':attribute should be exists:App\Models\Task,id',
        'date.*'    =>':attribute should be date',
    ];
    public function my_validate($inputs, $validateType){
        switch ($validateType) {
            case 'create':
                $validateRules = collect($this->validateRulesCreate);
                break;
            case 'update':
                $validateRules = collect($this->validateRulesUpdate);
                break;
            default:
                $validateRules = collect($this->validateRulesCreate);
                break;
        }
        $validator = Validator::make(
            $inputs,
            $validateRules->toArray(),
            $this->validateMessages
        );
        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['status' => false, 'ret' => $error];
        }
        return ['status' => true];
    }
 
    public function check_input(Request $request, $date='now')
    {
        $readyTo = collect($request->get('readyTo'));
        $date = new \Datetime($date);
        $date = $date -> format("Y-m-d");
        //validate
        $validated = $this->my_validate($request->all(), "update");
        if (!$validated['status']) {
            $this -> resp['ret'] = $validated['ret'];
            return $this -> resp;
        }
        $this -> resp['status'] = true;
        $this -> resp['ret'] = "check_input success";
        return $this -> resp;
    }

    public function wrong_SQL_test(Request $request)
    {
        try{
            $ret = \DB::table('day_things')
                ->where('dates', '2021-01-01')
                ->update(['dateq' => '2021-01-02']);
        } catch (\Exception $e){
            $this -> resp['message'] = $e->getMessage();
            $ret = false;
        }
        if ($ret){
            $this -> resp['status'] = true;
            $this -> resp['ret'] = $ret;
        } else {
            $this -> resp['ret'] = "cant update";
        }
        return $this -> resp;
    }
}
