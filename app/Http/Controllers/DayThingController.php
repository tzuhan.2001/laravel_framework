<?php

namespace App\Http\Controllers;

use App\Models\DayThing;
use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class DayThingController extends Controller
{
    protected $actionThreshold = 6;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, $year, $month=NULL, $day=NULL, $display = NULL)
    {
        $day_thing = [];
        try {
            $request->merge([
                'year' => $year,
                'month' => $month,
                'day' => $day,
            ]);
            $request->validate([
                'year' => 'nullable|date_format:Y',
                'month' => 'nullable|date_format:m',
                'day' => 'nullable|date_format:d',
            ]);
            

            $memo = DayThing::getMemoYMD($year, $month, $day);
            $done = Todo::getDoneYMD($year, $month, $day)
                        -> map(function($x){ 
                            $x->action = $x->task_info->map(function($action){
                                return $action->only(['id','name','comment']);
                            });
                            $x->action->transform(function($y){
                                if (is_array($y['comment']))
                                    $y['comment_count'] = count($y['comment']);
                                else
                                    $y['comment_count'] = 0;
                                $y['has_note'] = $y['comment_count'] > 0;
                                return $y;
                            });
                            $x->actionCount = count($x->action);
                            $x->actionCheck = $x->actionCount >= $this->actionThreshold; //done equal/more than 6 actions
                            return $x -> only(['date','action','actionCount','actionCheck']);
                        })
                        ;
            $dates = $memo->pluck('date')->merge($done->pluck('date'))->unique()->values();
            $dates -> map (function($date) use($memo, $done, &$day_thing, $display){
                $day_thing [$date] ??= [];
                $list = $memo->where('date',$date)-> pluck('memo') -> first();
                $day_thing [$date]['memoCheck'] = sizeof($list??[]) > 0;
                $day_thing [$date]['actionCheck'] = $done->where('date',$date)-> pluck('actionCount')->first() >= $this->actionThreshold;
                switch ($display) {
                    case 'simple':
                        break;
                    default:
                        $day_thing [$date]['memo'] = $memo->where('date',$date)-> pluck('memo')->first() ?? [];
                        $day_thing [$date]['action'] = $done->where('date',$date)-> pluck('action')->first() ?? [];
                        break;
                }
            });
            // sort by date
            $day_thing = collect($day_thing)->sortByDesc(function($x,$key){
                return $key;
            });
            return [
                'status' => true,
                'ret' => $day_thing,
            ];
            } catch (ValidationException $e){
            return [
                'status' => false,
                'ret' => $e->getMessage(),
                "message" => [
                    'request' => $request->all()
                ],
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "show error",
                "message" => $e->getMessage(),
            ];
        }
    }
    public function simple_show(Request $request, $year, $month=NULL, $day=NULL)
    {
        return $this->show($request, $year, $month, $day, 'simple');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DayThing $dayThing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */

    public function update(Request $request, $date='now')
    {
        try {
            $request->validate([
                'memo' => 'nullable|array',
                'memo.*' => 'nullable|string',
            ]);
            $date = new \Datetime($date);
            $date = $date -> format("Y-m-d");
            $day_thing = DayThing::firstOrNew(['user_id' => auth()->user()->id,'date'=>$date]);
            $day_thing->fill($request->all());
            $day_thing -> save();
            return [
                'status' => true,
                'ret' => true,
                // 'ret' => $day_thing,
            ];
        } catch (ValidationException $e){
            return [
                'status' => false,
                'ret' => $e->getMessage(),
                "message" => [
                    'request' => $request->all()
                ],
            ];
        } catch (\Exception $e){
            return [
                'status' => false,
                'ret' => "update error",
                "message" => $e->getMessage(),
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DayThing $dayThing)
    {
        //
    }
}
