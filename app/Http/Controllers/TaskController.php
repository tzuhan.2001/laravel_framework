<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $tasks = Task::user()->get();
            return $tasks;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $root = $request->get('root');
        $parent = $request->get('parent');
        //check root is exist or not
        $task1 = Task::user()->where('id', $root)->where('level', 0);
        // check parent is exist or not
        $task2 = Task::user()->where('id', $parent);
        if ($root <= 0 && $parent <= 0) {
            // setup project (no root & parent)
        } else if (!$task1->exists() || !$task2->exists()) {
            //both root& parent should be exist
            return [
                'status' => false,
                'ret' => 'root/parent is not allowed',
            ];
        }

        $request['user_id'] = auth()->user()->id;
        try {
            $task = Task::create($request->all());
            return [
                'status' => true,
                'ret' => $task,
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        try {
            $this->authorize('view', $task);
            return $task;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function update(Request $request, Task $task)
    {
        try {
            $this->authorize('update', $task);
            $task->fill($request->all());
            if ($task->status == 1) {
                $task->done_at = now();
                $task->progress = 1;
            } else if ($task->status == 0) {
                $task->done_at = null;
            } else if ($task->status == 2) {
                $task->done_at = null;
            }
            $task->save();
            return ['status' => true, 'ret' => true];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
        $this->authorize('delete', $task);
        /** TODO processes
         * 1. get all children
         * 2. get all todo, day_thing
         * 3. delete day_thing -> todo -> task
         */
        $task_id = $task->id;
        try{
            $ret = $task -> delete();
        } catch (\Exception $e){
            throw $e;
        }
        // return $deleted ? $task_id: 0;
        if ($ret)
            return ['status' => $ret, 'ret' => $task_id];
        return ['status' => $ret, 'ret' => "cannot delete Task $task_id"];
    }
}
