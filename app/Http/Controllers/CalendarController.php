<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $data = Calendar::all();
            return [
                "status" => true,
                "ret" => $data
            ];
        } catch (\Exception $e) {
            return [
                "status" => false,
                "ret" => "index failed",
                "message" => $e->getMessage()
            ];
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, $year, $month=NULL, $day=NULL)
    {
        try {
            $request->merge([
                'year' => $year,
                'month' => $month,
                'day' => $day,
            ]);
            $request->validate([
                'year' => 'nullable|date_format:Y',
                'month' => 'nullable|date_format:m',
                'day' => 'nullable|date_format:d',
            ]);
            $data = Calendar:: getHolidaysYMD($year, $month, $day);
            return [
                "status" => true,
                "ret" => $data
            ];
        } catch (ValidationException $e) {
            return [
                "status" => false,
                "ret" => "validation failed",
                "message" => $e->getMessage()
            ];
        } catch (\Exception $e) {
            return [
                "status" => false,
                "ret" => "show failed",
                "message" => $e->getMessage()
            ];
        }
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Calendar $calendar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Calendar $calendar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Calendar $calendar)
    {
        //
    }
}
