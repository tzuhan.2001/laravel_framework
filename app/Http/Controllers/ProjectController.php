<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Project;
use App\Models\Mission;
use App\Models\Action; // Import the Action class from the appropriate namespace
use Illuminate\Http\Request;

use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Validator;

class ProjectController extends TaskController
{
    protected $resp = [
        "status" => false,
        "ret" => null
    ];
    protected $validateRulesCreate = [
        // 'id'            =>'required|integer',
        'code'          =>'nullable|string',
        // 'root'          =>'required|integer|min:0|exclude_if:root,0|exists:App\Models\Task,id', //tree root. 0 means it is root
        // 'parent'        =>'required|integer|min:0|exclude_if:parent,0|exists:App\Models\Task,id', // 0 means it is root
        // 'level'         =>'required|integer|min:0', // 0: project; 1: mission; 2: action
        // 'user_id'       =>'required|integer',
        'name'          =>'required|string',
        'description'   =>'nullable|string',
        // 'begin_date'    =>'required|date',
        // 'end_date'      =>'required|date',
        'priority'      =>'nullable|digits_between:0,2',//has default value
        'progress'      =>'nullable|numeric|between:0,1',//has default value
        'status'        =>'nullable|digits_between:0,2',//has default value
        'done_at'       =>'nullable|integer', // timestamp is int
        'comment'       => 'nullable|array',
        'comment.*'     => 'nullable|string',
    ];
    protected $validateRulesUpdate = [
        'code'          =>'nullable|string',
        'root'          =>'nullable|integer|min:0|exclude_if:root,0|exists:App\Models\Task,id', //tree root. 0 means it is root
        'parent'        =>'nullable|integer|min:0|exclude_if:parent,0|exists:App\Models\Task,id', // 0 means it is root
        'level'         =>'nullable|integer|min:0', // 0: project; 1: mission; 2: action
        'user_id'       =>'nullable|integer',
        'name'          =>'nullable|string',
        'description'   =>'nullable|string',
        'begin_date'    =>'nullable|date',
        'end_date'      =>'nullable|date',
        'priority'      =>'nullable|digits_between:0,2',//has default value
        'progress'      =>'nullable|numeric|between:0,1',//has default value
        'status'        =>'nullable|digits_between:0,2',//has default value
        'done_at'       =>'nullable|integer', // timestamp is int
        'comment'       => 'nullable|array',
        'comment.*'     => 'nullable|string',
    ];
    protected $validateMessages = [
        'id.*'            =>':attribute should be required|integer',
        'code.*'          =>':attribute should be string',
        'root.*'          =>':attribute should be required|integer|exclude_if:root,0|exists:App\Models\Task,id',
        'parent.*'        =>':attribute should be required|integer|exclude_if:parent,0|exists:App\Models\Task,id',
        'level.*'         =>':attribute should be required|integer',
        'user_id.*'       =>':attribute should be required|integer',
        'name.*'          =>':attribute should be required|string',
        'description.*'   =>':attribute should be string',
        'begin_date.*'    =>':attribute should be date',
        'end_date.*'      =>':attribute should be date',
        'priority.*'      =>':attribute should be digits_between:0,2',
        'progress.*'      =>':attribute should be numeric|between:0,1',
        'status.*'        =>':attribute should be digits_between:0,2',
        'done_at.*'       =>':attribute should be timestamp',
        'comment.*'       =>':attribute should be array',
        'comment.*.*'     =>':attribute should be string',
    ];
    public function project_validate($inputs, $validateType){
        switch ($validateType) {
            case 'create':
                $validateRules = collect($this->validateRulesCreate);
                break;
            case 'update':
                $validateRules = collect($this->validateRulesUpdate);
                break;
            default:
                $validateRules = collect($this->validateRulesCreate);
                break;
        }
        $validator = Validator::make(
            $inputs,
            $validateRules->toArray(),
            $this->validateMessages
        );
        if ($validator->fails()) {
            throw new \InvalidArgumentException($validator->errors()->first());
            $error = $validator->errors()->first();
            return ['status' => false, 'ret' => $error];
        }
        return ['status' => true];
    }
    /** Project *///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public function project_index()
    {
        try {
            $tasks = Project::user()->get();
            return [
                'status' => true,
                'ret' => $tasks
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => $tasks,
            ];
        }
    }
    public function project_show(Project $project)
    {
        $this-> authorize('view', $project);
        $project->actions = $project->children;
        unset($project->children);
        return [
            'status' => true,
            'ret' => $project
        ];
    }
    public function project_store(Request $request)
    {

        try {
            //validate
            $validated = $this->project_validate($request->all(), "create");
            //new object
            $project = Project::create($request->all());
            return [
                'status' => true,
                'ret' => $project->id
            ];
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => [
                    'request' => $request->all()
                ],
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant create project(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    public function project_update(Request $request, Project $project)
    {
        try {
            //validate
            $validated = $this->project_validate($request->all(), "update");
            //update object
            $resp = parent::update($request, $project);
            return $resp;
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => [
                    'request' => $request->all()
                ],
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant update project(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    public function project_destroy(Project $project)
    {
        // record which tasks are deleted
        $project_id = $project->id;
        $deleted = $project->where('root',$project->id)->pluck('id')->toArray();
        try {
            // delete all children
            Task::user()->where('root',$project->id)->delete();
            // delete the task
            $resp = parent::destroy($project);
            
            $deleted = array_merge((array)$deleted, (array)$project_id);
            if ($resp['status']){
                // show result
                return [
                    'status' => true,
                    'ret' => $deleted
                ];
            } else {
                return $resp;
            }
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => $project_id,
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant delete project(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    /** Project Mission*///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public function project_mission_index()
    {
        try {
            $mission = Mission::user()->get();
            return [
                'status' => true,
                'ret' => $mission
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant show mission(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    public function project_mission_show(Mission $mission)
    {
        $this-> authorize('view', $mission);
        $mission->actions = $mission->children;
        unset($mission->children);
        return [
            'status' => true,
            'ret' => $mission
        ];
    }
    public function project_mission_store(Request $request)
    {
        try {
            //validate
            $validated = $this->project_validate($request->all(), "create");
            //new object
            $mission = Mission::create($request->all());
            updateProgress($mission->parent);
            return [
                'status' => true,
                'ret' => $mission->id
            ];
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => [
                    'request' => $request->all()
                ],
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant create mission(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    public function project_mission_update(Request $request, Mission $mission)
    {
        try {
            //validate
            $validated = $this->project_validate($request->all(), "update");
            //update object
            $resp = parent::update($request, $mission);
            return $resp;
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => [
                    'request' => $request->all()
                ],
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => $mission,
            ];
        }
    }
    public function project_mission_update_actions(Request $request, Task $mission)
    {
        $user_id = auth()->user()->id;
        // get children
        $requestCollect = $request->collect();
        $requestCollect -> transform(function($child) use($mission, $user_id) {
            $child['user_id'] = $user_id;
            $child['root'] = $mission->root;
            $child['parent'] = $mission->id;
            $child['level'] = 2;
            return $child;
        });
        $missionChildren = $mission->children;

        //create action from request
        $createChildren = $requestCollect->filter( function($task, $key){
                                return empty($task['id']);
                            })->values();
        //update by request
        $updateChildren = $missionChildren -> whereIn('id', $requestCollect -> pluck('id') -> toArray())
                            ->values();
        //delete no exist actions
        $deleteChildren = $missionChildren -> whereNotIn('id', $requestCollect -> pluck('id') -> toArray())
                            ->values();
        $createResp = $createChildren -> transform(function($child) use($mission, $user_id) {
            try {
                $child['validate'] = $this->project_validate($child, "create");
                $action = Action::create($child);
                return [
                    'status' => true,
                    'ret' => $action->id
                ];
            } catch (\InvalidArgumentException $e){
                return [
                    'status' => false,
                    'message' => [
                        'createChildren' => $child
                    ],
                    'ret' => $e->getMessage(),
                ];
            } catch (\Exception $e) {
                return [
                    'status' => false,
                    'message' => $e->getMessage(),
                    'ret' => "cant create action(". __FUNCTION__ . " " . __LINE__ .")",
                ];
            }
        });
        $updateResp =  $updateChildren -> transform(function($child) use($mission, $requestCollect) {
            try {
                $updateRequest = $requestCollect->firstWhere('id', $child->id);
                $child['validate'] = $this->project_validate($updateRequest, "update");
                $ret_bool = Action::find($child['id'])->update($updateRequest);
                return [
                    'status' => $ret_bool,
                    'ret' => $child['id'],
                    // 'updateRequest' => $updateRequest,
                    // 'action' => Action::find($child['id'])->first(),
                ];
            } catch (\InvalidArgumentException $e){
                return [
                    'status' => false,
                    'message' => [
                        'updateChildren' => $updateRequest
                    ],
                    'ret' => $e->getMessage(),
                ];
            } catch (\Exception $e) {
                return [
                    'status' => false,
                    'message' => $e->getMessage(),
                    'ret' => "cant update action(". __FUNCTION__ . " " . __LINE__ .")",
                ];
            }
        });
        //delete object
        $deleteResp = $deleteChildren -> map(function($child){
            try {
                return parent::destroy($child);
            } catch (\Exception $e) {
                return [
                    'status' => false,
                    'message' => $e->getMessage(),
                    'ret' => "cant delete action(". __FUNCTION__ . " " . __LINE__ .")",
                ];
            }
        });
        
        try{
            //update progress
            updateProgress($mission->id);
            return [
                'status' => true,
                'ret' => [
                    'create' => $createResp,
                    'update' => $updateResp,
                    'delete' => $deleteResp,
                ]
            ];
        } catch(\Exception $e){
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant update progress(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    public function project_mission_destroy(Task $mission)
    {
        $mission_id = $mission->id;
        $parent_id = $mission->parent;

        $deleted = $mission->children->pluck('id')->toArray();
        try {
            // delete all children
            Task::user()->where('parent',$mission_id)->delete();
            // delete the task
            $resp = parent::destroy($mission);
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => $mission_id,
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant delete mission(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
        //update Progress
        try{
            updateProgress($parent_id);
            if ($resp['status']){
                // show result
                $deleted = array_merge((array)$deleted, (array)$mission_id);
                return [
                    'status' => true,
                    'ret' => $deleted
                ];
            } else {
                return $resp;
            }
        } catch(\Exception $e){
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant update progress(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    /** Project Action*///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public function project_action_index()
    {
        try {
            $action = Action::user()->get();
            return [
                'status' => true,
                'ret' => $action
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant show action(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    public function project_action_show(Action $action)
    {
        $this-> authorize('view', $action);
        return [
            'status' => true,
            'ret' => $action
        ];
    }
    public function project_action_store(Request $request)
    {
        try {
            //validate
            $validated = $this->project_validate($request->all(), "create");
            //new object
            $action = Action::create($request->all());
            updateProgress($action->parent);
            return [
                'status' => true,
                'ret' => $action->id
            ];
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => [
                    'request' => $request->all()
                ],
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant create action(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    public function project_action_update(Request $request, Action $action)
    {
        try {
            //validate
            $validated = $this->project_validate($request->all(), "update");
            //update object
            $resp = parent::update($request, $action);
            updateProgress($action->parent);
            return $resp;
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => [
                    'request' => $request->all()
                ],
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => $action,
            ];
        }
    }
    public function project_action_destroy(Action $action)
    {
        $action_id = $action->id;
        $parent_id = $action->parent;

        $deleted = $action->children->pluck('id')->toArray();
        try {
            // delete all children
            Task::user()->where('parent',$action_id)->delete();
            // delete the task
            $resp = parent::destroy($action);
        } catch (\InvalidArgumentException $e){
            return [
                'status' => false,
                'message' => $action_id,
                'ret' => $e->getMessage(),
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant delete action(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
        //update Progress
        try{
            updateProgress($parent_id);
            if ($resp['status']){
                // show result
                $deleted = array_merge((array)$deleted, (array)$action_id);
                return [
                    'status' => true,
                    'ret' => $deleted
                ];
            } else {
                return $resp;
            }
        } catch(\Exception $e){
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant update progress(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    /** Project Progress *///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    public function project_progress_index()
    {
        try {
            $projects = Project::user()->get();
            $projects->transform(function ($item, $key) {
                $resp = $this->project_progress_show($item);
                return $resp['ret'][0];
            });
            return [
                'status' => true,
                'ret' => $projects->values()
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant show progress(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
    
    public function project_progress_show(Project $project)
    {
        $this->authorize('view', $project);
        // next action by deadline
        try{
            $actions = $project->action;
            $next_action = 0;
            $max_action_score = -INF;
            foreach ($actions as $key => $action){
                $score = scoreOfTask($action, $method = "deadline");
                if($score > $max_action_score){
                    $max_action_score = $score;
                    $next_action = $action->id;
                }
                // -INF can't be json
                $action->score = is_infinite($score) ? -99999 : $score;
            }
            $project -> next_action = Action::find($next_action);
            $project -> max_action_score = is_infinite($max_action_score) ? -99999 : $max_action_score;
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant show progress(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
        // next children(mission) by deadline
        try{
            $next_child = 0;
            $max_score = -INF;
            $children = $project->children;
            foreach ($children as $key => $child){
                // $score = scoreOfTask($child);
                $score = scoreOfTask($child, $method = "deadline");
                if($score > $max_score){
                    $max_score = $score;
                    $next_child = $child->id;
                }
                // -INF can't be json
                $child->score = is_infinite($score) ? -99999 : $score;
            }
            $project -> next_child = Mission::find($next_child);
            $project -> max_score = is_infinite($max_score) ? -99999 : $max_score;
            return [
                'status' => true,
                'ret' => [$project] //return array by request from frontend developer
            ];
        } catch (\Exception $e) {
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'ret' => "cant show progress(". __FUNCTION__ . " " . __LINE__ .")",
            ];
        }
    }
}
