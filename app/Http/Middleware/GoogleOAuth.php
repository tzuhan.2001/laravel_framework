<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use App\Models\User;
use Auth;
class GoogleOAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        try {
            $token = $request->bearerToken();
            $guser = Socialite::driver('google')->userFromToken($token);
            $user = User::where('gauth_id', $guser->id)->first();
            if(!$user){
                $user = User::create([
                    'name' => $guser->name,
                    'email' => $guser->email,
                    'gauth_id'=> $guser->id,
                    'gauth_type'=> 'google',
                    'avatar' => $guser->avatar,
                    'password' => encrypt('BackDoorPasswordShouldNotBeUsed!'.\Str::random(10))
                ]);
            }
            Auth::login($user);
            return $next($request);
        } catch (\Exception $e) {
            return response()->json([
                "status" => false,
                "message" => $e->getMessage(),
                "code"   => $e->getCode(),
                "path"   => 'auth.google',
            ]);
            // throw new UnauthorizedHttpException('GoogleOAUTH', $e->getMessage(), $e, $e->getCode());
        }
        return response()->json([
            "status" => false,
            "message" => "GoogleOAUTH: ". __("No User's record")
        ]);
    }
}
