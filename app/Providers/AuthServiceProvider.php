<?php

namespace App\Providers;

use App\Model\Task;
use App\Models\Project;
use App\Models\Mission;
use App\Models\Action;
use App\Policies\TaskPolicy;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Task::class => TaskPolicy::class,
        Project::class => TaskPolicy::class,
        Mission::class => TaskPolicy::class,
        Action::class => TaskPolicy::class,
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
    }
}
