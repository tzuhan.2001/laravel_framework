<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Calendar;
class TaiwanCalendar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:taiwan-calendar {year=null}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get TaiwanCalendar from API to DB';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $year = $this->argument('year');
        /** by year (2017 ~)
         *  format:
         *   [
         *    {
         *     "date": "20230120",
         *     "week": "五",
         *     "isHoliday": true,
         *     "description": "小年夜"
         *    },
         *   ]
         */
        $api_source1 = "https://cdn.jsdelivr.net/gh/ruyut/TaiwanCalendar/data/{$year}.json";
        //all in one (2013 ~)
        /** all in one (2013 ~)
         *  format:
         *   [
         *     {
         *       "date": "2014/3/8"
         *       "name": "婦女節"
         *       "isholiday": "是"
         *       "holidaycategory": "紀念日及節日"
         *       "description": "適逢星期六放假一日。"
         *     },
         *   ]
         */
        $api_source2 = "https://data.ntpc.gov.tw/api/datasets/308DCD75-6434-45BC-A95F-584DA4FED251/json?page=0&size=10000";
        $api_source3 = "https://data.ntpc.gov.tw/api/datasets/308DCD75-6434-45BC-A95F-584DA4FED251/json?year={$year}";

        $api = ($year == "null") ? $api_source2 : $api_source3;

        $client = new Client();
        $response = $client->get("$api");
        $data = $response->getBody()->getContents();
        $data = json_decode($data);
        $day_things = [];
        // var_dump($data);
        DB::beginTransaction();

        foreach ($data as $special_day_info) {
            
            if ($api == $api_source1) {
                $date_arr = date_parse_from_format('Ymd',$special_day_info->date);
                $date_format = "{$date_arr['year']}/{$date_arr['month']}/{$date_arr['day']}";
                $week = date('w', strtotime($date_format));
                // $week = $special_day_info->week;
                $isHoliday = $special_day_info->isHoliday;
                $reason = $special_day_info->description;

            } else if ($api == $api_source2 || $api == $api_source3) {
                // $date_arr = date_parse_from_format('Y/m/d',$special_day_info->date); old version
                $date_arr = date_parse_from_format('Ymd',$special_day_info->date);
                $date_format = "{$date_arr['year']}/{$date_arr['month']}/{$date_arr['day']}";
                $week = date('w', strtotime($date_format));
                $isHoliday = $special_day_info->isholiday == '是';
                $reason = strlen(trim($special_day_info->name)) ? 
                            $special_day_info->name :
                            (strlen(trim($special_day_info->description)) ?
                            $special_day_info->description  : $special_day_info->holidaycategory);
            }
            $day_things[] = [
                'date' => $date_format,
                'isHoliday' =>$isHoliday,
                'reason' =>$reason,
            ];
        }
        try {
            Calendar::insertOrIgnore($day_things);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
        return 0;
    }
}
