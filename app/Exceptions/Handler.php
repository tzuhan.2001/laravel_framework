<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            // $exception = new \Illuminate\Database\Eloquent\ModelNotFoundException('The requested model was not found.');
            return response()->json([
                'status' => 'false',
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'ret' => "The requested model was not found.(ModelNotFoundException)",
            ], 404);
        }
        if ($exception instanceof \Illuminate\Auth\Access\AuthorizationException) {
            return response()->json([
                'status' => 'false',
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
                'ret' => "The requested model was not found.(AuthorizationException)",
            ], 403);
        }
        // else if ($exception instanceof \InvalidArgumentException) {
        //     return response()->json([
        //         'status' => 'false',
        //         'message' => $exception->getMessage(),

        //     ], 200);
        // } else if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
        //     return response()->json([
        //         'status' => 'false',
        //         'message' => $exception->getMessage(),

        //     ], 200);
        // } else if ($exception instanceof \Illuminate\Validation\ValidationException) {
        //     return response()->json([
        //         'status' => 'false',
        //         'message' => $exception->getMessage(),

        //     ], 200);
        // }
        else if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return response()->json([
                'status' => 'false',
                'message' => $exception->getMessage(),
                'code' => $exception->getStatusCode(),
                'ret' => "The requested model was not found.(NotFoundHttpException)",

            ], 404);
        } 
        // else if ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
        //     return response()->json([
        //         'status' => 'false',
        //         'message' => $exception->getMessage(),

        //     ], 200);
        // } else if ($exception instanceof \Illuminate\Auth\Access\AuthorizationException) {
        //     return response()->json([
        //         'status' => 'false',
        //         'message' => $exception->getMessage(),

        //     ], 200);
        // } else if ($exception instanceof \Illuminate\Database\QueryException) {
        //     return response()->json([
        //         'status' => 'false',
        //         'message' => $exception->getMessage(),

        //     ], 200);
        // } 
        else if ($exception instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException) {
            return response()->json([
                'status' => 'false',
                'message' => $exception->getMessage(),
                'code' => $exception->getStatusCode(),
                'ret' => "The requested model was not found.(UnauthorizedHttpException)",
                // 'error' => 'Unauthorized',
                // 'error_description' => 'The request requires valid user authentication.',
                // 'msg' => get_class($exception),
            ], 401);
        }
    
        return parent::render($request, $exception);
    }
}
