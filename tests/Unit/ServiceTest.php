<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

use App\Models\Task;
use App\Models\Project;
use App\Models\Mission;
use App\Models\Action;
use App\Models\Todo;
use App\Models\DayThing;
use App\Models\Calendar;
use App\Http\Controllers\ProjectController;

class ServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function test_TaskModel(): void
    {
        $task = new Task();
        $this->assertInstanceOf(Task::class, $task);
        $this->assertEquals($task->level, 0);
    }
    public function test_ProjectModel(): void
    {
        $project = new Project();
        $this->assertInstanceOf(Project::class, $project);
        $this->assertEquals($project->level, 0);
    }
    public function test_MissionModel(): void
    {
        $mission = new Mission();
        $this->assertInstanceOf(Mission::class, $mission);
        $this->assertEquals($mission->level, 1);
    }
    public function test_ActionModel(): void
    {
        $action = new Action();
        $this->assertInstanceOf(Action::class, $action);
        $this->assertEquals($action->level, 2);
    }
    public function test_TodoModel(): void
    {
        $todo = new Todo();
        $this->assertInstanceOf(Todo::class, $todo);
    }
    public function test_DayThingModel(): void
    {
        $dayThing = new DayThing();
        $this->assertInstanceOf(DayThing::class, $dayThing);
    }
    public function test_CalendarModel(): void
    {
        $calendar = new Calendar();
        $this->assertInstanceOf(Calendar::class, $calendar);
    }

    // public function test_ProjectController(): void
    // {
    //     $projectController = new ProjectController();
    //     $this->assertInstanceOf(ProjectController::class, $projectController);
    // }
}
