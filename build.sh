#!/bin/sh

#Submodule Setting
git submodule init
git submodule update --remote

alias SAIL='./vendor/bin/sail'
#Env Setting
SAIL up -d #start up docker compose
sleep 10
# laravel
SAIL composer install
SAIL art key:generate
SAIL art jwt:secret -f #create JWT key
sleep 10
#laravel
SAIL art migrate #build tables
#build calendar
SAIL art app:taiwan-calendar
#DB for test
SAIL art db:seed
